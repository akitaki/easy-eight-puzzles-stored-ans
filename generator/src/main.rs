use std::collections::{HashMap, HashSet, VecDeque};
use std::convert::TryInto;

use indicatif::ParallelProgressIterator;
use itertools::Itertools;
use rayon::prelude::*;
use rand::prelude::*;

fn main() {
    let mut perms = (0..9).permutations(9).collect_vec();
    perms.shuffle(&mut thread_rng());
    let answers: Vec<_> = perms
        .par_iter()
        .take(10000)
        .progress_count(10000)
        .filter_map(|perm| {
            let perm: [i32; 9] = perm[..].try_into().unwrap();
            let state = State::new(perm);
            let value = state.value();
            if value != 15 {
                Some(encode(perm, value))
            } else {
                None
            }
        })
        .collect();

    println!("Writing {} records to file", answers.len());
    std::fs::write("./out.txt", answers.iter().join("")).expect("Failed to write to file");
}

fn encode(perm: [i32; 9], value: usize) -> String {
    let mut perm_num = 0;
    for &i in perm.iter().rev() {
        perm_num *= 10;
        perm_num += i;
    }

    let mut res = vec![];
    while perm_num > 0 {
        res.push(char_for(perm_num % 32176));
        perm_num /= 32176;
    }
    res.reverse();
    res.push(char::from_u32(value as u32 + 97).unwrap());
    res.iter().join("")
}

fn char_for(num: i32) -> char {
    // 0~20991 -> CJK (4e00 ~ 9fff)
    // 20992~32175 -> Hangul (ac00 ~ d7af)
    match num {
        0..=20991 => char::from_u32(num as u32 + 0x4e00).unwrap(),
        20992..=32175 => char::from_u32(num as u32 - 20992 + 0xac00).unwrap(),
        _ => panic!("indice OOB"),
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone)]
struct State {
    data: [i32; 9],
}

impl State {
    fn new(data: [i32; 9]) -> Self {
        Self { data }
    }

    fn value(&self) -> usize {
        let mut checked = HashSet::<Self>::new();
        let mut front = VecDeque::<(Self, usize)>::new();
        checked.insert(self.clone());
        front.push_back((self.clone(), 0));

        while let Some((state, step)) = front.pop_front() {
            if step == 15 {
                return 15;
            }
            if state.is_end() {
                return step;
            }

            for next_state in state.next_states() {
                if !checked.contains(&next_state) {
                    checked.insert(next_state.clone());
                    front.push_back((next_state, step + 1));
                }
            }
        }

        panic!("Shouldn't reach");
    }

    fn is_end(&self) -> bool {
        const END: [i32; 9] = [1, 2, 3, 4, 5, 6, 7, 8, 0];
        self.data == END
    }

    fn next_states(&self) -> impl Iterator<Item = Self> + '_ {
        const TO_SWAP: [&[usize]; 9] = [
            &[1, 3],
            &[0, 2, 4],
            &[1, 5],
            &[0, 4, 6],
            &[1, 3, 5, 7],
            &[2, 4, 8],
            &[3, 7],
            &[4, 6, 8],
            &[5, 7],
        ];
        let zero_pos = self
            .data
            .iter()
            .position(|&num| num == 0)
            .expect("Unable to find zero");
        TO_SWAP[zero_pos].iter().map(move |&i| {
            let mut state = self.clone();
            state.data.swap(zero_pos, i);
            state
        })
    }
}
